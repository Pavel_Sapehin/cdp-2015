﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginInjector
{
    using System.Linq.Expressions;

    using PluginInjector.AppDomainManager;
    using PluginInjector.FileSystemHelpers;
    using PluginInjector.PluginInformation.Interfaces;

    public class SimplePluginInjector: IPluginInjector
    {
        /// <summary>
        /// The plugins.
        /// </summary>
        private Dictionary<Type, object> plugins = new Dictionary<Type, object>();

        private IPluginScanner scanner { get; }

        /// <summary>
        /// The founded plugins.
        /// </summary>
        private Dictionary<Type, List<IPluginInformationExtended>> foundedPlugins = new Dictionary<Type, List<IPluginInformationExtended>>();

        /// <summary>
        /// Register plugin which will be added to scan
        /// </summary>
        /// <param name="predicate">IPluginInfo match expression</param>
        /// <param name="pluginFolder">Folder in which search for plugins will be made</param>
        /// <typeparam name="T">Interface - the type of plugin interface</typeparam>
        public void RegisterPluginInterface<T>(Expression<Func<IPluginInformation, bool>> predicate, string pluginFolder = "/plugins/") where T : class
        {
            this.scanner.RegisterPluginInterface<T>(predicate, pluginFolder);
        }

        public SimplePluginInjector(IPluginScanner scanner)
        {
            this.scanner = scanner;
        }

        public void SetDefaultVersion<T>(string version)
        {
            throw new NotImplementedException();
        }

        public T GetPlugin<T>()
        {
            throw new NotImplementedException();
        }

        public T GetPlugin<T>(string version)
        {
            throw new NotImplementedException();
        }

        public IPluginInformation GetPluginInformation<T>()
        {
            throw new NotImplementedException();
        }

        public void RegisterPlugin<T>()
        {
            throw new NotImplementedException();
        }

        public void RegisterPlugin<T>(string path)
        {
            throw new NotImplementedException();
        }
    }
}
