﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginInjector.AppDomians.Infrastructure
{
    using System.IO;
    using System.Linq.Expressions;
    using System.Reflection;

    using PluginInjector.AppDomainManager;
    using PluginInjector.AppDomians.Scanner;
    using PluginInjector.PluginInformation;
    using PluginInjector.PluginInformation.Interfaces;

    internal class PluginScannerProxy: MarshalByRefObject
    {
        private Dictionary<Type, Tuple<string, Expression<Func<IPluginInformation, bool>>>> _pluginRegister = new Dictionary<Type, Tuple<string, Expression<Func<IPluginInformation, bool>>>>();

        /// <summary>
        /// The register plugins interfaces.
        /// </summary>
        /// <param name="pluginRegister">
        /// The plugin Register.
        /// </param>
        /// <exception cref="ArgumentException">
        /// When argument null or empty.
        /// </exception>
        public void RegisterPluginsInterfaces(Dictionary<Type, Tuple<string, Expression<Func<IPluginInformation, bool>>>> pluginRegister)
        {
            if (pluginRegister == null || pluginRegister.Count <= 0)
            {
                throw new ArgumentException(nameof(pluginRegister));
            }

            this._pluginRegister = pluginRegister;
        }

        /// <summary>
        /// Specifies path to plugins
        /// </summary>
        /// <param name="pluginsPath">
        /// Path to plug-ins folder.
        /// Must be empty to use current folder.
        /// Must start with / (slash) to use relative path
        /// Must start with NO / (slash) to use absolute path
        /// </param>
        protected FileInfo[] GetAssembliesFileInfos(string pluginsPath)
        {
            try
            {
                // If path started with '/' then path started from current directory
                DirectoryInfo directory;
                if (pluginsPath[0] == '/')
                {
                    var currentDirectoryPath = Directory.GetCurrentDirectory();
                    directory = new DirectoryInfo(currentDirectoryPath + pluginsPath);
                }
                else directory = new DirectoryInfo(pluginsPath);
                return directory.GetFiles("*.dll");
            }
            catch (Exception ex)
            {
                //leave it for now
            }

            return null;
        }

        protected List<IPluginInformationExtended> GetAssemblyTypes(FileInfo assemblyFileInfo, Type pluginInterfaceType, Expression<Func<IPluginInformation, bool>> matchExpression)
        {
            if (!pluginInterfaceType.IsInterface) throw new NotSupportedException("You need to pass an interface in order to get it from an assembly");
            List<IPluginInformationExtended> foundedPluginsInfo = new List<IPluginInformationExtended>();
            var assembly = Assembly.LoadFrom(assemblyFileInfo.FullName);
            Type[] types = assembly.GetTypes();
            foreach (Type type in types)
            {
                try
                {
                    if (pluginInterfaceType.IsAssignableFrom(type) && type.GetPluginAssemblyAttribute() != null)
                    {
                        var attribute = type.GetPluginAssemblyAttribute();
                        if (matchExpression.Compile()(attribute))
                        {
                            var pluginExtendedInformation = new PluginInformationExtended(
                                attribute,
                                assembly.FullName,
                                pluginInterfaceType,
                                type);
                            foundedPluginsInfo.Add(pluginExtendedInformation);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //TODO: Log exceptions
                }
            }
            
            return foundedPluginsInfo;
        }

        public Dictionary<Type, List<IPluginInformationExtended>> ScanForPlugins()
        {
            Dictionary<Type, List<IPluginInformationExtended>> foundedInformation = new Dictionary<Type, List<IPluginInformationExtended>>();
            foreach (var plugin in this._pluginRegister)
            {
                var pluginInterface = plugin.Key;
                var path = plugin.Value.Item1;
                var matchExpression = plugin.Value.Item2;
                var assemblies = this.GetAssembliesFileInfos(path);
                foreach (var pathInfo in assemblies)
                {
                    var plugins = this.GetAssemblyTypes(pathInfo, pluginInterface, matchExpression);
                    foundedInformation.Add(pluginInterface, plugins);
                }
            }
            return foundedInformation;
        } 
    }
}
