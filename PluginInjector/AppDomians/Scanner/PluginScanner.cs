﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginInjector.FileSystemHelpers
{
    using System.Linq.Expressions;

    using PluginInjector.AppDomainManager;
    using PluginInjector.AppDomians.Infrastructure;
    using PluginInjector.AppDomians.Scanner;
    using PluginInjector.PluginInformation.Interfaces;

    //TODO: Add exception handling\logging

    /// <summary>
    /// The assembly scanner.
    /// </summary>
    public class PluginScanner : IPluginScanner
    {
        /// <summary>
        /// The _plugin register.
        /// </summary>
        private readonly Dictionary<Type, Tuple<string, Expression<Func<IPluginInformation, bool>>>> pluginRegister = new Dictionary<Type, Tuple<string, Expression<Func<IPluginInformation, bool>>>>();

        /// <summary>
        /// Register plugin which will be added to scan
        /// </summary>
        /// <param name="predicate">IPluginInfo match expression</param>
        /// <param name="pluginFolder">Folder in which search for plugins will be made</param>
        /// <typeparam name="T">Interface - the type of plugin interface</typeparam>
        public void RegisterPluginInterface<T>(Expression<Func<IPluginInformation, bool>> predicate, string pluginFolder = "/plugins/") where T : class
        {
            var interfaceType = typeof(T);
            var folderPredicatePair = new Tuple<string, Expression<Func<IPluginInformation, bool>>>(
                pluginFolder,
                predicate);
            this.pluginRegister.Add(interfaceType, folderPredicatePair);
        }

        /// <summary>
        /// The scan for plugins.
        /// </summary>
        /// <param name="pluginsInformation">
        /// The plugins information.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/>.
        /// </returns>
        public Dictionary<Type, List<IPluginInformationExtended>> ScanForPlugins(IEnumerable<IPluginInformation> pluginsInformation)
        {
            var temporaryAppDomain = AppDomain.CreateDomain("TemporaryDomain");
            PluginScannerProxy pluginScanner = (PluginScannerProxy)temporaryAppDomain.CreateInstanceAndUnwrap(typeof(PluginScannerProxy).Assembly.FullName, "PluginScannerProxy");
            pluginScanner.RegisterPluginsInterfaces(this.pluginRegister);
            var result = pluginScanner.ScanForPlugins();
            //TODO: Add exception handling
            AppDomain.Unload(temporaryAppDomain);
            return result;
        }
    }
}
