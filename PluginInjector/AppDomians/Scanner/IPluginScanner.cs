namespace PluginInjector.FileSystemHelpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;

    using PluginInjector.AppDomainManager;
    using PluginInjector.PluginInformation.Interfaces;

    public interface IPluginScanner
    {
        /// <summary>
        /// Register plugin which will be added to scan
        /// </summary>
        /// <param name="predicate">IPluginInfo match expression</param>
        /// <param name="pluginFolder">Folder in which search for plugins will be made</param>
        /// <typeparam name="T">Interface - the type of plugin interface</typeparam>
        void RegisterPluginInterface<T>(Expression<Func<IPluginInformation, bool>> predicate, string pluginFolder = "/plugins/") where T : class;

        /// <summary>
        /// The scan for plugins.
        /// </summary>
        /// <param name="pluginsInformation">
        /// The plugins information.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary{TKey,TValue}"/>.
        /// </returns>
        Dictionary<Type, List<IPluginInformationExtended>> ScanForPlugins(IEnumerable<IPluginInformation> pluginsInformation);
    }
}