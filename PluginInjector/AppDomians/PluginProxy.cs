﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginInjector.AppDomians.Exceptions
{
    public class PluginProxy<T>: MarshalByRefObject
    {
        public T Instance { get; }
    }
}
