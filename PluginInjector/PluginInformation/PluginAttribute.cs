﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginInjector.PluginInformation
{
    using PluginInjector.AppDomainManager;

    /// <summary>
    /// Mark class with this attribute to use it as plug-in
    /// Also contains information about plug-in
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class PluginAttribute : Attribute, IPluginInformation
    {
        public string FriendlyName { get; }

        public Guid UniqueId { get; }

        /// <summary>
        /// Gets the version of the plug-in
        /// </summary>
        public string Version { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginAttribute"/>.</summary>
        /// <param name="version">Version of plug-in</param>
        public PluginAttribute(string version, string friendlyName, Guid uniqueId)
        {
            Version = version;
            this.FriendlyName = friendlyName;
            this.UniqueId = uniqueId;
        }
    }
}
