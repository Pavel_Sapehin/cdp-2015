﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginInjector.PluginInformation.Interfaces
{
    using System.Reflection;

    using PluginInjector.AppDomainManager;

    /// <summary>
    /// Must returned from an <c>PluginScanner</c>
    /// </summary>
    public interface IPluginInformationExtended : IPluginInformation
    {
        string AssemblyFullName { get; }
        Type PluginInterfaceType { get; }
        Type PluginImplementationType { get; }
    }
}
