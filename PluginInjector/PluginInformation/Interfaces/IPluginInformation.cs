namespace PluginInjector.AppDomainManager
{
    using System;

    using PluginInjector.PluginInformation.Interfaces;

    /// <summary>
    /// The PluginInformation interface.
    /// TODO: Implement searching in GAC
    /// </summary>
    public interface IPluginInformation
    {
        string FriendlyName { get; }
        Guid UniqueId { get; }
        string Version { get; }
    }
}