﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginInjector.AppDomians.Scanner
{
    using PluginInjector.AppDomainManager;
    using PluginInjector.PluginInformation.Interfaces;
    public class PluginInformationExtended: IPluginInformationExtended
    {
        private PluginInformationExtended(IPluginInformation pluginInformation)
        {
            this.FriendlyName = pluginInformation.FriendlyName;
            this.UniqueId = pluginInformation.UniqueId;
            this.Version = pluginInformation.Version;
        }

        public PluginInformationExtended(IPluginInformation pluginInformation, string assemblyFullName, Type pluginInterfaceType, Type pluginImplementationType): this(pluginInformation)
        {
            this.AssemblyFullName = assemblyFullName;
            this.PluginInterfaceType = pluginInterfaceType;
            this.PluginImplementationType = pluginImplementationType;
        }

        public PluginInformationExtended(Guid id, string friendlyName, string version)
        {
            this.UniqueId = id;
            this.FriendlyName = friendlyName;
            this.Version = version;
        }

        public PluginInformationExtended(Guid id, string friendlyName, string version, string assemblyFullName, Type pluginInterfaceType, Type pluginImplementationType): this(id, friendlyName, version)
        {
            this.AssemblyFullName = assemblyFullName;
            this.PluginInterfaceType = pluginInterfaceType;
            this.PluginImplementationType = pluginImplementationType;
        }

        public string FriendlyName { get; }

        public Guid UniqueId { get; }

        public string Version { get; }

        public string AssemblyFullName { get; }

        public Type PluginInterfaceType { get; }

        public Type PluginImplementationType { get; }
    }
}
