﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginInjector.PluginInformation
{
    /// <summary>
    /// Contains helper static methods for PluginAssemblyAttribute.
    /// </summary>
    public static class AttributeRetriever
    {
        public static PluginAttribute GetPluginAssemblyAttribute(this Type target)
        {
            PluginAttribute pluginAssemblyAttribute = null;
            try
            {
                pluginAssemblyAttribute = (PluginAttribute)Attribute.GetCustomAttribute(target, typeof(PluginAttribute));
            }
            catch (Exception e) 
            {
                //TODO: Handle exceptions more precisely, log it if needed
                //Do nothing as I do not want to now whether exception occured or not for now
            }

            return pluginAssemblyAttribute;
        }
    }
}
