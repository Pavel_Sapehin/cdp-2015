﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginInjector.AppDomainManager
{
    using System.Linq.Expressions;

    public interface IPluginInjector
    {
        /// <summary>
        /// Set the default version which will be used if there is several versions.
        /// </summary>
        /// <param name="version">Default version to be set</param>
        /// <typeparam name="T">Type of plug in for which default version will be configured</typeparam>
        void SetDefaultVersion<T>(string version);

        /// <summary>
        /// Loads configured plug in dynamically
        /// </summary>
        /// <typeparam name="T">Type of plug-in</typeparam>
        /// <returns>The instance of plug in<see cref="T"/>.</returns>
        T GetPlugin<T>();

        /// <summary>
        /// Get plugin with specified version
        /// </summary>
        /// <param name="version">Version of the plugin</param>
        /// <typeparam name="T">Type of plug-in</typeparam>
        /// <returns></returns>
        T GetPlugin<T>(string version);

        /// <summary>
        /// Return information about registered plug-in
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        IPluginInformation GetPluginInformation<T>();

        /// <summary>
        /// The register Register default version of interface as plug-in.
        /// All plug-ins must have an PluginAssemblyAttribute in order to be loaded.
        /// </summary>
        /// <typeparam name="T">Type of plug-in</typeparam>
        void RegisterPlugin<T>();

        /// <summary>
        /// The register plugin.
        /// </summary>
        /// <param name="path">Default path for the plug-in, where assemblies will be scanned</param>
        /// <typeparam name="T">Type of plug-in</typeparam>
        void RegisterPlugin<T>(string path);

        /// <summary>
        /// Register plugin which will be added to scan
        /// </summary>
        /// <param name="predicate">IPluginInfo match expression</param>
        /// <param name="pluginFolder">Folder in which search for plugins will be made</param>
        /// <typeparam name="T">Interface - the type of plugin interface</typeparam>
        void RegisterPluginInterface<T>(
            Expression<Func<IPluginInformation, bool>> predicate,
            string pluginFolder = "/plugins/") where T : class;
    }
}
