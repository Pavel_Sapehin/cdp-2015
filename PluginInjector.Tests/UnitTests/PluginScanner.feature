﻿Feature: PluginScanner
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers

@PluginScanner
Scenario: PluginScanner shoud correctly oad Assembly in AppDomain, scan for required interface
	Given I have created AppDomain with name="TemporaryAppDomain"
	And I have created instance of PluginScanner<IPluginInterface> in that AppDomain
	And I have an assembly which contains Type which implement IPluginInterface 
	And I place it a current directory subfolder="/default"
	When I call Scan() method of PluginScanner instance
	Then It must return string with full name of founded type and its version
