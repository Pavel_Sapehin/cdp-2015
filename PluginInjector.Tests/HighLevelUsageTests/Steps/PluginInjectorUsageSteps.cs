﻿using System;
using TechTalk.SpecFlow;

namespace PluginInjector.Tests.HighLevelUsageTests.Steps
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PluginInjector.AppDomainManager;
    using PluginInjector.PluginInformation;
    using PluginInjector.Tests.HighLevelUsageTests.Mocks;

    using TechTalk.SpecFlow.Assist;

    [Binding]
    public class PluginInjectorUsageSteps
    {
        [Given(@"I have an instance of PluginInjector")]
        public void GivenIHaveAnInstanceOfPluginInjector()
        {
            IPluginInjector pluginInjector = new SimplePluginInjector();
            ScenarioContext.Current.Set(pluginInjector, "pluginInjector");
        }

        [Given(@"I did not register plugin interface in PluginInjector")]
        public void GivenIDidNotRegisterPluginInterfaceInPluginInjector()
        {
            //do not call pluginInjector.RegisterPlugin
        }

        [Then(@"it must throw an exeption with message=""(.*)""")]
        public void ThenItMustThrowAnExeptionWithMessage(string p0)
        {
            var exception = ScenarioContext.Current.Get<string>("exceptionMessage");
            Assert.AreEqual(exception, p0);
        }

        [Given(@"I have registered plugin interface in the PluginInjector with default settings")]
        public void GivenIHaveRegisteredPluginInterfaceInThePluginInjectorWithDefaultSettings()
        {
            IPluginInjector pluginInjector = ScenarioContext.Current.Get<IPluginInjector>("pluginInjector");
            //default settings when no parameters are presented
            pluginInjector.RegisterPlugin<IPluginInterface>();
        }

        [Given(@"plugin is not presented in the file system")]
        public void GivenPluginIsNotPresentedInTheFileSystem()
        {
            //do not create instance of IPluginInterface on the disk in default path
        }

        [When(@"I try to get plugin with default configuration")]
        public void WhenITryToGetPluginWithDefaultConfiguration()
        {
            this.TryGetPlugin();
        }

        [Given(@"I set default plugin version=(.*)")]
        public void GivenISetDefaultPluginVersion(Decimal p0)
        {
            var pluginInjector = ScenarioContext.Current.Get<IPluginInjector>("pluginInjector");
            pluginInjector.SetDefaultVersion<IPluginInjector>(p0.ToString());
        }

        [Given(@"plugin presented in the file system with version=(.*)")]
        public void GivenPluginPresentedInTheFileSystemWithVersion(Decimal p0)
        {
            var pluginPath = "/plugins/";
            var mockAssemblyGenerator = new MockAssemblyGenerator("PluginInterfaceAssembly" + p0.ToString() + ".dll", p0.ToString(), pluginPath);
            var isAssemblyGenerated = mockAssemblyGenerator.GenerateAssembly();
            if (!isAssemblyGenerated) throw new Exception("Assembly for test is not generated");
        }

        [When(@"I try to get plugin with version=(.*)")]
        public void WhenITryToGetPluginWithVersion(Decimal p0)
        {
            this.TryGetPlugin(p0.ToString());
        }

        [Given(@"have several plugins with different versions")]
        public void GivenHaveSeveralPluginsWithDifferentVersions(Table table)
        {
            
            foreach (TableRow row in table.Rows)
            {
                var version = row["Version"];
                var assemblyCreator = new MockAssemblyGenerator("MockAssembly" + version, version, "/plugins/");
                var isAssemblyCreated = assemblyCreator.GenerateAssembly();
                if(!isAssemblyCreated) throw new Exception("Assemblies for tests are not generated.");
            }
        }

        [Then(@"it must return an appropriate plugin instance with (.*)")]
        public void ThenItMustReturnAnAppropriatePluginInstanceWith(Decimal p0)
        {
            var plugin = ScenarioContext.Current.Get<IPluginInterface>(p0.ToString());
            var pluginAttribute = plugin.GetType().GetPluginAssemblyAttribute();
            Assert.AreEqual(p0.ToString(), pluginAttribute.Version);
        }

        /// <summary>
        /// Try get plug-in from plug-in injector, set exception if needed
        /// </summary>
        /// <param name="version">Version of plugin to get. Set to null to call GetPlugin without parameters</param>
        private void TryGetPlugin(string version = null)
        {
            var pluginInjector = ScenarioContext.Current.Get<IPluginInjector>("pluginInjector");
            IPluginInterface plugin = null;
            string exceptionMessage = null;
            try
            {
                if (string.IsNullOrEmpty(version))
                {
                    plugin = pluginInjector.GetPlugin<IPluginInterface>();
                }

                plugin = pluginInjector.GetPlugin<IPluginInterface>(version);
            }
            catch (Exception e)
            {
                exceptionMessage = e.Message;
            }

            ScenarioContext.Current.Set(plugin, "plugin");
            ScenarioContext.Current.Set(exceptionMessage, "exceptionMessage");
        }
    }
}
