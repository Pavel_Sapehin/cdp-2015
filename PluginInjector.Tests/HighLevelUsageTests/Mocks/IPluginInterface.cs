﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginInjector.Tests.HighLevelUsageTests.Mocks
{
    public interface IPluginInterface
    {
        void SetSomething(dynamic something);
        dynamic GetSomething();

        string StringProperty { get; set; }
    }
}
