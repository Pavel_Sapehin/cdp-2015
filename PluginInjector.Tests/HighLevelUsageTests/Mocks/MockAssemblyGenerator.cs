﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginInjector.Tests.HighLevelUsageTests.Mocks
{
    using System.CodeDom.Compiler;

    public class MockAssemblyGenerator
    {
        private string _relativePath = null;
        private string _assemblyName = null;
        private string _version = null;

        public MockAssemblyGenerator(string assemblyName, string version, string relativePath = "/plugins")
        {
            this._relativePath = relativePath;
            this._assemblyName = assemblyName;
            this._version = version;
        }

        /// <summary>
        /// The generate Plugin assembly for IPluginInterface
        /// TODO: Add possibility to choose interface, move to another class generation\configuration
        /// </summary>
        /// <returns>True if assembly successfully generated</returns>
        public bool GenerateAssembly()
        {
            try
            {
                System.CodeDom.Compiler.CompilerParameters parameters = new CompilerParameters();
                parameters.GenerateExecutable = false;
                parameters.OutputAssembly = _relativePath + _assemblyName;

                var assemblyCode = @"
                namespace PluginInjector.Tests.HighLevelUsageTests.Mocks
                {
                    using PluginInjector.PluginInformation;

                    [PluginAssembly(""" + _version + @""")]
                    public class MockAssembly : IPluginInterface
                    {
                        private dynamic _something;
                        public void SetSomething(dynamic something)
                        {
                            this._something = something;
                        }

                        public dynamic GetSomething()
                        {
                            return this._something;
                        }

                        public string StringProperty { get; set; } = ""This is default string property"";
                    }
                }";
                CompilerResults result = CodeDomProvider.CreateProvider("CSharp")
                    .CompileAssemblyFromSource(parameters, assemblyCode);
                return !result.Errors.HasErrors;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
