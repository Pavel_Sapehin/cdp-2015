﻿Feature: PluginInjectorUsage
	In order to check general top-level functionality of PluginInjector

@PluginInjector
Scenario: Getting plugin without registering plugin interface must throw an exception.
	Given I have an instance of PluginInjector
	And I did not register plugin interface in PluginInjector
	When I try to get plugin with default configuration
	Then it must throw an exeption with message="This plugin interface is not registered."

@PluginInjector
Scenario: Getting plugin which is configured but not assembly is not exist must throw an exeption.
	Given I have an instance of PluginInjector
	And I have registered plugin interface in the PluginInjector with default settings
	And plugin is not presented in the file system
	When I try to get plugin with default configuration
	Then it must throw an exeption with message="There is no plugins found for the configuration."

@PluginInjector
Scenario: Getting a specifiec plugin version must throw an exception if there is no a such version.
	Given I have an instance of PluginInjector
	And I have registered plugin interface in the PluginInjector with default settings
	And I set default plugin version=0.01
	And plugin presented in the file system with version=0.01
	When I try to get plugin with version=0.02
	Then it must throw an exeption with message="Plugin exist but has no requested version."

@PluginInjector
Scenario Outline: Getting plugin which has several versions in a file system must return plugin instance with requested version.
	Given I have an instance of PluginInjector
	And I have registered plugin interface in the PluginInjector with default settings
	And I set default plugin version=0.01
	And have several plugins with different versions
	| Version |
	| 0.01    |
	| 0.02    |
	When I try to get plugin with version <RequestedVersion>
	Then it must return an appropriate plugin instance with <ActualVersion>

	Examples: 
	| RequestedVersion | ActualVersion |
	| 0.01             | 0.01          |
	| 0.02             | 0.02          |
	| empty            | 0.01          |